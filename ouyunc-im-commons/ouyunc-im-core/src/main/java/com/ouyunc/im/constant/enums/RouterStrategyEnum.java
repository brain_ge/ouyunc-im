package com.ouyunc.im.constant.enums;

/**
 * @Author fangzhenxun
 * @Description: 路由策略枚举
 **/
public enum RouterStrategyEnum {

    /**
     * 随机
     */
    RANDOM,

    /**
     * 回溯
     */
    BACKTRACK
}
